// Refactor
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

type WikiQuery struct {
	Query WikiSearch `json:"query"`
}

type WikiSearchInfo struct {
	TotalHits int `json:"totalhits"`
}

type WikiSearch struct {
	Search []Result       `json:"search"`
	Hits   WikiSearchInfo `json:"searchinfo"`
}

type Result struct {
	Title string `json:"title"`
}

func Wikipedia(term string) WikiQuery {
	url, err := url.Parse("https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch=Craig%20Noone&format=json&srlimit=1")
	if err != nil {
		fmt.Println(err)
	}
	q := url.Query()
	q.Set("srsearch", term)
	url.RawQuery = q.Encode()
	r, err := http.Get(url.String())
	if err != nil {
		fmt.Println(err)
	}
	content, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println(err)
	}
	var reading WikiQuery
	err = json.Unmarshal(content, &reading)
	if err != nil {
		fmt.Println(err)
	}
	return reading
}
