## Goals:
1. Wikipedia search capability
2. Log user messages so I can do some statistics on it by text analysis.
3. Ability to mark messages as sticky (this will increase the default pinning of discord of 50 messages).
