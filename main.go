package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/bwmarrin/discordgo"
)

// Variables used for command line parameters

func main() {

	Token := os.Getenv("Token")
	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + Token)
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}

	// Register the messageCreate func as a callback for MessageCreate events.
	//dg.AddHandler()

	// Open a websocket connection to Discord and begin listening.
	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	dg.Close()
}

// Refactor
/*
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if strings.HasPrefix(m.Content, "!wiki") {
		message, err := s.ChannelMessage(m.ChannelID, m.ID)
		if err != nil {
			fmt.Println(err)
		}
		index := strings.Index(message.Content, " ") + 1
		searchTerm := message.Content[index:]

		wikiResult := Wikipedia(searchTerm)
		termValue := strings.ReplaceAll(wikiResult.Query.Search[0].Title, " ", "_")
		resulturl := "https://en.wikipedia.org/wiki/" + termValue

		fmt.Println(wikiResult)
		fmt.Println(resulturl)
		s.ChannelMessageSend(m.ChannelID, resulturl)
	}
}
*/
